﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LineDrawer : MonoBehaviour {
	[SerializeField] Transform[] m_Points;
	[SerializeField] float m_NodesRadius;
	[SerializeField] Color m_NodeColor;
	[SerializeField] Color m_LineColor;

	void OnDrawGizmos() {
		Gizmos.color = m_NodeColor;
		for (int i = 0; i < m_Points.Length; i ++) {
			if (m_Points[i] == null) continue;
			Gizmos.DrawSphere(m_Points[i].position, m_NodesRadius);
		}

		Gizmos.color = m_LineColor;
		for (int i = 1; i < m_Points.Length; i ++) {
			if (m_Points[i] == null || m_Points[i-1] == null) continue;
			Gizmos.DrawLine(m_Points[i - 1].position, m_Points[i].position);
		}
	}
}
