﻿using UnityEngine;
using System.Collections;

public class IKChain : MonoBehaviour {
	[SerializeField] Transform[] m_IKNodes;
	[SerializeField] Transform m_IKTarget;

	Quaternion[] m_DefaultRotations;
	float[] m_ArmLength;
	public float m_MaxLimbLength;
	public float m_DefaultLimbLength;

	// Use this for initialization
	void Start () {
		m_DefaultRotations = new Quaternion[m_IKNodes.Length];
		m_ArmLength = new float[m_IKNodes.Length - 1];
		m_MaxLimbLength = 0;

		for (int i = 0; i < m_IKNodes.Length; i ++) {
			m_DefaultRotations[i] = m_IKNodes[i].rotation;
		}

		for (int i = 1; i < m_IKNodes.Length; i ++) {
			m_ArmLength[i - 1] = Vector3.Distance(m_IKNodes[i - 1].position, m_IKNodes[i].position);
			m_MaxLimbLength += m_ArmLength[i - 1];
		}

		m_DefaultLimbLength = Vector3.Distance(m_IKNodes[0].position, m_IKNodes[m_IKNodes.Length - 1].position);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		
	}
}
