﻿using UnityEngine;
using System.Collections;

public class CrossIK : MonoBehaviour {
	[SerializeField] Transform[] m_Nodes = new Transform[4];
	[SerializeField] Transform m_IKTarget;
	[SerializeField] float m_Trend = 0.5f;

	Quaternion[] m_DefaultRotations = new Quaternion[4];
	float[] m_ArmLength = new float[4];
	public float m_MaxLimbLength;
	public float m_MinLimbLength;
	public float m_DefaultLimbLength;
	public float m_UpperProportion;
	public float m_LowerProportion;
	const int k_Thigh = 0;
	const int k_Knee = 1;
	const int k_Heel = 2;
	const int k_Ankle = 3;

	public float upperAngle;
	public float lowerAngle;
	public float offset;

	// Use this for initialization
	void Start () {
		m_MaxLimbLength = 0;

		for (int i = 0; i < 4; i ++) {
			m_DefaultRotations[i] = m_Nodes[i].rotation;
		}

		for (int i = 0; i < 3; i ++) {
			m_ArmLength[i] = Vector3.Distance(m_Nodes[i].position, m_Nodes[i + 1].position);
			m_MaxLimbLength += m_ArmLength[i];
		}

		m_MinLimbLength = m_ArmLength[k_Thigh] + m_ArmLength[k_Heel] - m_ArmLength[k_Knee];

		m_UpperProportion = m_ArmLength[k_Thigh] / (m_ArmLength[k_Thigh] + m_ArmLength[k_Heel]);
		m_LowerProportion = m_ArmLength[k_Heel] / (m_ArmLength[k_Thigh] + m_ArmLength[k_Heel]);

		m_DefaultLimbLength = Vector3.Distance(m_Nodes[k_Thigh].position, m_Nodes[k_Ankle].position);
	}

	// Update is called once per frame
	void LateUpdate () {
		float fToTarget = Vector3.Distance(m_Nodes[k_Thigh].position, m_IKTarget.position);
		fToTarget = Mathf.Clamp(fToTarget, m_MinLimbLength + 0.00001f, m_MaxLimbLength - 0.00001f);

		float upperLimb = fToTarget * m_UpperProportion;
		float lowerLimb = fToTarget * m_LowerProportion;

		float upperCalf = m_ArmLength[k_Knee] * m_UpperProportion;
		float lowerCalf = m_ArmLength[k_Knee] * m_LowerProportion;

		float adjacent = (m_ArmLength[k_Thigh] * m_ArmLength[k_Thigh] - upperCalf * upperCalf + upperLimb*upperLimb) / (2*upperLimb);
		upperAngle = Mathf.Acos(adjacent/m_ArmLength[k_Thigh]) * Mathf.Rad2Deg;

		adjacent = (m_ArmLength[k_Thigh] * m_ArmLength[k_Thigh] + upperCalf * upperCalf - upperLimb*upperLimb) / (2*upperCalf);
		lowerAngle = Mathf.Acos(adjacent/m_ArmLength[k_Thigh]) * Mathf.Rad2Deg;

		m_Nodes[k_Thigh].localRotation = Quaternion.Euler(upperAngle, 0, 0);
		m_Nodes[k_Knee].localRotation = Quaternion.Euler(180-lowerAngle, 0, 0);
		m_Nodes[k_Heel].localRotation = Quaternion.Euler(lowerAngle-180, 0, 0);

		Vector3 dToTarget = m_IKTarget.position - m_Nodes[k_Thigh].position;
		Vector3 dToFoot = m_Nodes[k_Ankle].position - m_Nodes[k_Thigh].position;
		float upperoffset = Vector3.Angle(dToTarget, dToFoot) * Mathf.Sign(Vector3.Cross(dToTarget, dToFoot).x);

//		print("middle:" + (180 - upperAngle - lowerAngle));
//		print("offset:" + upperoffset);
//		print("Distance: " + fToTarget);
//		print("RealDist: " + dToFoot.magnitude);
//		Debug.DrawLine(m_Nodes[k_Thigh].position, m_Nodes[k_Ankle].position, Color.green);
//		Debug.DrawLine(m_Nodes[k_Thigh].position, m_IKTarget.position, Color.red);
		m_Nodes[k_Thigh].localRotation = Quaternion.Euler(upperAngle - upperoffset, 0, 0);
	}
}