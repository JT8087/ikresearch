﻿using UnityEngine;
using System.Collections;


// Current performance: 8192 Instances / 0.5~3ms per frame, many spikes, highest to 8ms, very unstable(machine problem?)
// Test CPU: Inter i7-4710mq

// Current performance: 8192 Instances / 3~6ms per frame, stable
// Test CPU: Inter i5-4210h
public class JointConstraint : MonoBehaviour {
	
	[Range(0, 90)][SerializeField]float m_Q1 = 20;
	[Range(0, 90)][SerializeField]float m_Q2 = 20;
	[Range(0, 90)][SerializeField]float m_Q3 = 20;
	[Range(0, 90)][SerializeField]float m_Q4 = 20;
	[SerializeField]bool m_Reverse = false;
	[SerializeField]bool m_AngleSolve = false;

	// Many global variables, yeah, for fewer "new"s
	float distance;
	float _Q1L, _Q2L, _Q3L, _Q4L;
	float _QX, _QY;

	Vector3 toTarget, relativeTarget;
	Vector3 projectPoint3D, newPoint3D;
	Vector2 newPoint2D;

	Mesh m_Cone;
	Transform m_NormalRepresent;

	void OnDrawGizmos() {
		if (m_NormalRepresent == null) m_NormalRepresent = transform;

		_Q1L = Mathf.Sin(m_Q1 * Mathf.Deg2Rad);
		_Q2L = Mathf.Sin(m_Q2 * Mathf.Deg2Rad);
		_Q3L = Mathf.Sin(m_Q3 * Mathf.Deg2Rad);
		_Q4L = Mathf.Sin(m_Q4 * Mathf.Deg2Rad);
		MakeMesh();
		Gizmos.color = new Color(0, 1, 1, 0.5f);
		Gizmos.DrawWireMesh(m_Cone, m_NormalRepresent.position, m_NormalRepresent.rotation);
	}

	// Initialize.
	void Awake() {
		if (m_NormalRepresent == null) m_NormalRepresent = transform;

		_Q1L = Mathf.Sin(m_Q1 * Mathf.Deg2Rad);
		_Q2L = Mathf.Sin(m_Q2 * Mathf.Deg2Rad);
		_Q3L = Mathf.Sin(m_Q3 * Mathf.Deg2Rad);
		_Q4L = Mathf.Sin(m_Q4 * Mathf.Deg2Rad);
	}

	/// <summary>
	/// Constrains the joint position. Make joint inside limit-cone and 
	/// is at correct distance between target and joint.
	/// </summary>
	/// <returns>The joint position.</returns>
	/// <param name="targetPosition">Target position.</param>
	/// <param name="desiredDistance">Desired distance.</param>
	public Vector3 ConstrainJointPosition(Vector3 targetPosition, float desiredDistance) {
		
		// I will need more knowledge of geometry in order to improve this...eh
		if (m_NormalRepresent == null) m_NormalRepresent = transform;

		Debug.DrawLine(m_NormalRepresent.position, targetPosition, Color.black);

		// Get the vector from joint to target.
		toTarget = targetPosition - m_NormalRepresent.position;

		// Get the relative position of target.
		relativeTarget = m_NormalRepresent.InverseTransformPoint(targetPosition);

		// Find out which quadrant this point is in
		FindOutQuadrant(relativeTarget);

		// Get the distance between target and joint
		distance = toTarget.magnitude;

		// Check if the project point is inside the ellipse defined by Q1, Q2, Q3, and Q4.
		if (!isInsideEllipse(distance, relativeTarget)) {
			if (m_AngleSolve) {
				return AngleToPoint(AngleWithSign(Vector2.right, relativeTarget));
			}

			// Solve for the new point.
			newPoint2D = ProjectPointToEllipseEdge(relativeTarget);
			newPoint3D.Set(newPoint2D.x, newPoint2D.y, 0);

			// We knew how far this point should be...
			newPoint3D.z = Mathf.Sqrt(1 - newPoint2D.sqrMagnitude);
			newPoint3D *= desiredDistance;
		} else {
			newPoint3D = toTarget.normalized * desiredDistance;
		}
			
		Debug.DrawRay(m_NormalRepresent.position, newPoint3D, Color.cyan);
		return (newPoint3D);
	}
		
	void FindOutQuadrant(Vector2 point) {
		_QX = point.x > 0? _Q1L: _Q3L;
		_QY = point.y > 0? _Q2L: _Q4L;
	}

	bool isInsideEllipse(float distance, Vector3 point) {

		// if the point is behind ellipse then just return false
		if (point.z < 0) return false;

		float a = _QX * distance;
		float b = _QY * distance;

		// From http://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
		float answer = (point.x * point.x) / (a * a) + (point.y * point.y) / (b * b);

		return answer <= 1;
	}

	// This is a makeshift version. Not accurate, but should be close enough
//	Vector2 ClosestPointOfEllipse(Vector2 point) {
//		bool XBiggerThanY = _QX > _QY;
//		bool IsLine = XBiggerThanY? _QY==0: _QX==0;
//
//		Vector2 per = point;
//		if (XBiggerThanY) {
//			per.y = 0;
//			if (per.x > _QX) {
//				per.x = _QX;
//			}
//		} else {
//			per.x = 0;
//			if (per.y > _QY) {
//				per.y = _QY;
//			}
//		}
//
//		float k1 = point.y / point.x;
//		float k2 = 
//
//		if (IsLine) {
//			
//		}
//	}

	// project yes, but thiat is not the closest point to target.
	Vector2 ProjectPointToEllipseEdge(Vector2 point) {

		// Define a line which cross original point and the target point.
		// The implicit equation of line is: 
		//		y = c*x
		// And then c = y/x
		float c = point.y / point.x;

		// Handle some possible calculation problems.
		// When y = 0...
		if (float.IsNaN(c)) {
			c = 0;
		// When x = 0...
		} else if (float.IsInfinity(c)) {
			c = float.IsPositiveInfinity(c)?  100000: -100000;
		}


		// Solve these two equations to get the new point.
		// (x^2)/(a^2) + (y^2)/(b^2) = 1			(1)
		// y = c*x;									(2)
		// 
		// Put (2) in (1) to get (3):
		// (x^2)/(a^2) + ((c*x)^2)/(b^2) = 1		(3)
		//
		// Simplify (3): 
		// x = sqrt((a^2 * b^2)/(b^2 + a^2 * c^2))	(4)
		// 
		// Solve (4), and put x back to (2) to get y
		Vector2 newPoint;
		float powerOfa = _QX * _QX;
		float powerOfb = _QY * _QY;
		float powerOfc = c*c;
		float u = powerOfb + powerOfa * powerOfc;
		float l = powerOfa * powerOfb;
		float ans = Mathf.Sqrt(l/u);

		// There will be two cross-points between the ellipse and the line,
		// We should find which is the point we need by checking original point.
		newPoint.x = point.x < 0? -ans: ans;

		// And finally, getting y.
		newPoint.y = newPoint.x * c;

//		newPoint.Scale(new Vector2(_QX, _QY));

		// Return the new point.
		return newPoint;
	}

	// Make a cone mesh, just for editor
	void MakeMesh() {

		if (m_Cone == null) m_Cone = new Mesh();
		int Accuracy = 32;

		int TotalVert = 1 + 4 * Accuracy;
		Vector3[] m_Vertices = new Vector3[TotalVert];
		float rotateAxisAngle = 90.0f / Accuracy;

		m_Vertices[0] = Vector3.zero;
		for (int i = 1; i < TotalVert; i ++) {
			m_Vertices[i] = AngleToPoint(rotateAxisAngle * (i - 1)) * 3;
		}

		m_Cone.vertices = m_Vertices;

		Vector3[] normal = new Vector3[TotalVert];
		int[] Triangle = new int[(TotalVert-1) * 3];
		for (int i = 0; i < TotalVert - 1; i ++) {
			Triangle[i * 3] = 0;
			Triangle[i * 3 + 1] = i + 1;
			if (i == TotalVert - 2) {
				Triangle[i * 3 + 2] = 1;
				normal[i + 1] = Vector3.Cross(m_Vertices[1] - m_Vertices[i + 1], m_Vertices[0] - m_Vertices[i + 1]);
			} else {
				Triangle[i * 3 + 2] = i + 2;
				normal[i + 1] = Vector3.Cross(m_Vertices[i + 2] - m_Vertices[i + 1], m_Vertices[0] - m_Vertices[i + 1]);
			}
		}
		normal[0] = -Vector3.forward;
		m_Cone.normals = normal;
		m_Cone.triangles = Triangle;
	}

	float AngleWithSign(Vector2 From, Vector2 To) {
		float angle = Vector2.Angle(From, To);
		angle *= Mathf.Sign(From.x * To.y - From.y * To.x);
		return angle;
	}

	// This is only use to make a cone mesh
	Vector3 AngleToPoint(float angle) {

		Vector3 point = Vector3.zero;
		angle %= 360;
		angle = angle < 0? angle += 360: angle;

		if (angle < 90 || angle >= 270) {
			point.x = _Q1L;
		} else {
			point.x = _Q3L;
		}

		if (angle < 180) {
			point.y = _Q2L;
		} else {
			point.y = _Q4L;
		}

		angle *= Mathf.Deg2Rad;
		point.x *= Mathf.Cos(angle);
		point.y *= Mathf.Sin(angle);

		point.z = Mathf.Sqrt(1 - point.sqrMagnitude);
		if (float.IsNaN(point.z)) point.z = 0;

		return point;

	}

	public Transform NormalRespect {
		set {
			m_NormalRepresent = value;
		}
	}
}
