﻿using UnityEngine;
using System.Collections;

public class FABRIK : MonoBehaviour {
	enum IK_UP_Dir{
		UP,
		DOWN,
		LEFT,
		RIGHT,
		BACK,
		FORWARD
	}

	[SerializeField]Transform[] m_Nodes;		// The joints IK will need to rotate
	[SerializeField]Transform m_Target;
	[SerializeField]IK_UP_Dir m_UpDirection;
	[SerializeField]bool m_Solve;
	[SerializeField]bool m_ReturnToDefault;

	static Transform _IKFolder;

	public float[] m_Distances;
	public float m_MaxLength;
	public float m_Tol;
	public bool m_Step;

	public Vector3[] nodePositions;

	// TODO: Naming... Think up some better names...
	Transform[] m_IKNodes;				// These are the things IK really moving
	Transform[] m_JointRepresentor;		// Use to represent joint rotation
	Transform[] m_ConstrainRespect;		// Joint-Constraint's normal will respect to these.
	JointConstraint[] m_JointConstaints;

	Vector3[] m_DefaultPos;


	void Start() {
		m_MaxLength = 0;
		m_Distances = new float[NodeAmount -1];
		nodePositions = new Vector3[NodeAmount];

		for (int i = 0; i < NodeAmount - 1; i ++) {
			m_Distances[i] = Vector3.Distance(m_Nodes[i].position, m_Nodes[i + 1].position);
			m_MaxLength += m_Distances[i];
		}

		Initialize();
	}


	void LateUpdate() {
		if (m_Solve && m_Step) {
			reorderIKNodes();
			StartCoroutine(IKSolve());
			//IKSolve();
			m_Step = false;
		}

		for (int i = 0; i < NodeAmount - 1; i ++) {
			Debug.DrawLine(m_IKNodes[i].position, m_IKNodes[i + 1].position, Color.magenta);
			Debug.DrawRay(m_ConstrainRespect[i].position, m_ConstrainRespect[i].forward*55, Color.white);
		}
	}


	//void IKSolve() {
	IEnumerator IKSolve() {
		// Get the distance between target and root joint
		float fJointToTarget = Vector3.Distance(m_Nodes[0].position, m_Target.position);

		if (m_ReturnToDefault) {
			for (int i = 0; i < NodeAmount; i ++) {
				nodePositions[i] = m_DefaultPos[i];
			}
		} else {
			// Save current position of nodes for compute.
			for (int i = 0; i < NodeAmount; i ++) {
				nodePositions[i] = m_Nodes[i].position;
			}
		}

		// Check whether the target is within reach
		// Unreachable
		if (fJointToTarget > m_MaxLength) {
			for (int i = 0; i < NodeAmount - 1; i ++) {
				Debug.DrawRay(nodePositions[i], m_ConstrainRespect[i].forward*55, Color.white);
				if (m_JointConstaints[i] != null) {
					Vector3 dir = m_JointConstaints[i].ConstrainJointPosition(m_Target.position, m_Distances[i]);
					Debug.DrawRay(nodePositions[i], dir, Color.yellow);
					yield return null;
					nodePositions[i + 1] = nodePositions[i] + dir;
				} else
				{
					// Find the distance between the target 
					// and the joint position
					fJointToTarget = Vector3.Distance(nodePositions[i], m_Target.position);
					float fLambda = m_Distances[i] / fJointToTarget;

					// Find the new joint position
					nodePositions[i + 1] = (1 - fLambda) * nodePositions[i] + fLambda*m_Target.position;
				}
				m_IKNodes[i+1].position = nodePositions[i+1];
				Debug.DrawLine(m_IKNodes[i+1].position, m_IKNodes[i].position, Color.red);
				yield return null;
				redirectIKNode(i);
				yield return null;
				Debug.DrawRay(nodePositions[i], m_ConstrainRespect[i].forward*50, Color.magenta);
				yield return null;
			}
		// Reachable
		} else {
			// Target is reachable, thus, set as b the initial position
			// of the root joint
			Vector3 v3RootOriginalPos = nodePositions[0];

			// Check whether the distance between the end effector
			// and the target is greater than a tolerance
			float fDiff = Vector3.Distance(nodePositions[NodeAmount - 1], m_Target.position);
			//for (int b = 0; b < 3 && fDiff > m_Tol; b ++) {
			for (int b = 0; b < 1; b ++) {	
				// STAGE 1: FORWARD REACHING
				// Set the end effector as target
				nodePositions[NodeAmount - 1] = m_Target.position;
				for (int i = NodeAmount - 2; i >= 0; i --) {
					
					// Find the distance between the new joint position Node[i + 1] and the joint node[i]
					fJointToTarget = Vector3.Distance(nodePositions[i + 1], nodePositions[i]);
					float fLambda = m_Distances[i] / fJointToTarget;

					// Find the new joint position
					nodePositions[i] = (1 - fLambda) * nodePositions[i + 1] + fLambda * nodePositions[i];
					Debug.DrawLine(nodePositions[i], nodePositions[i + 1], Color.red);
					yield return null;
				}

				// Stage 2: BACKWARD REACHING
				// Set the root joint its initial position.
				nodePositions[0] = v3RootOriginalPos;
				for (int i = 0; i < NodeAmount - 1; i ++) {
					Debug.DrawRay(nodePositions[i], m_ConstrainRespect[i].forward*55, Color.white); 
					if (m_JointConstaints[i] != null) {
						Vector3 dir = m_JointConstaints[i].ConstrainJointPosition(nodePositions[i + 1], m_Distances[i]);
						Debug.DrawRay(nodePositions[i], dir, Color.yellow);
						yield return null;
						nodePositions[i + 1] = nodePositions[i] + dir;
					} else
					{
						// Find the distance between the new joint position Node[i] and the joint node[i + 1]
						fJointToTarget = Vector3.Distance(nodePositions[i + 1], nodePositions[i]);
						float fLambda = m_Distances[i] / fJointToTarget;

						// Find the new joint position
						nodePositions[i + 1] = (1 - fLambda) * nodePositions[i] + fLambda * nodePositions[i + 1];
					}
					m_IKNodes[i+1].position = nodePositions[i+1];
					Debug.DrawLine(m_IKNodes[i+1].position, m_IKNodes[i].position, Color.red);
					yield return null;
					redirectIKNode(i);
					yield return null;
					Debug.DrawRay(nodePositions[i], m_ConstrainRespect[i].forward*50, Color.magenta); 
					yield return null;
				}
				fDiff = Vector3.Distance(nodePositions[NodeAmount - 1], m_Target.position);
			}
		}

		for (int i = 0; i < NodeAmount; i ++) {
			m_Nodes[i].position = nodePositions[i];
			yield return null;
		}
	}

	void Initialize() {
		// Put all the IK nodes inside an folder(for better Hierarchy view.)
		if (_IKFolder == null) _IKFolder = new GameObject("IKFolder").transform;

		m_DefaultPos = new Vector3[NodeAmount];
		m_IKNodes = new Transform[NodeAmount];
		m_JointRepresentor = new Transform[NodeAmount];
		m_ConstrainRespect = new Transform[NodeAmount];
		m_JointConstaints = new JointConstraint[NodeAmount];

		for (int i = 0; i < NodeAmount; i ++) {
			// Find joint constaint if exist.
			m_JointConstaints[i] = m_Nodes[i].GetComponent<JointConstraint>();

			// Create IKNodes and put them under IKFolder
			m_IKNodes[i] = new GameObject("IKCase").transform;
			m_IKNodes[i].parent = _IKFolder;
		}

		// Put all the IKNodes to their desired positions.
		reorderIKNodes();

		for (int i = 0; i < NodeAmount; i ++) {
			// Create Joint representor and constraint respect
			m_JointRepresentor[i] = new GameObject("JointRep").transform;
			m_ConstrainRespect[i] = new GameObject("Constraint").transform;

			// Parent Joint representor to IKNode.
			// So they will move as IKNodes move.
			// And their rotation will be the same as joints
			// So when IK solved we can use their rotations to rotate joints
			m_JointRepresentor[i].parent = m_IKNodes[i];
			m_JointRepresentor[i].position = m_IKNodes[i].position;
			m_JointRepresentor[i].rotation = m_Nodes[i].rotation;

			if (i <= 0) {
				m_ConstrainRespect[i].parent = m_Nodes[0].parent;
			} else {
				m_ConstrainRespect[i].parent = m_IKNodes[i - 1];
			}
			m_ConstrainRespect[i].position = m_IKNodes[i].position;
			m_ConstrainRespect[i].rotation = m_IKNodes[i].rotation;
			if (m_JointConstaints[i] != null)
				m_JointConstaints[i].NormalRespect = m_ConstrainRespect[i];

			m_DefaultPos[i] = m_IKNodes[i].position;
		}
	}

	void reorderIKNodes() {
		for (int i = 0; i < NodeAmount; i ++) {
			m_IKNodes[i].position = m_Nodes[i].position;
		}

		for (int i = 0; i < NodeAmount - 1; i ++) {
			m_IKNodes[i].LookAt(m_IKNodes[i + 1], UpDirection);
		}
	}

	void redirectIKNode(int index) {
		m_IKNodes[index].LookAt(m_IKNodes[index + 1], UpDirection);
	}

	int NodeAmount {
		get { return m_Nodes.Length; }
	}

	// TODO: Should make an dynamic up-direction that respect to character's body.
	Vector3 UpDirection {
		get {
			Vector3 up = Vector3.left;

			switch (m_UpDirection) {
				case IK_UP_Dir.UP: 		up = Vector3.up; 		break;
				case IK_UP_Dir.DOWN: 	up = Vector3.down; 		break;
				case IK_UP_Dir.LEFT: 	up = Vector3.left; 		break;
				case IK_UP_Dir.BACK: 	up = Vector3.back; 		break;
				case IK_UP_Dir.RIGHT: 	up = Vector3.right; 	break;
				case IK_UP_Dir.FORWARD: up = Vector3.forward; 	break;
			}
			return up;
		}
	}
}
