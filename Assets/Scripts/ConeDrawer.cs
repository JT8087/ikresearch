﻿using UnityEngine;
using System.Collections;

// This is a prototype script, will be deleted in the future.

public class ConeDrawer : MonoBehaviour {
	[SerializeField]float m_Radius;
	[SerializeField]Vector3[] m_Vertices;
	[Range(0, 90)][SerializeField]float Q1 = 20;
	[Range(0, 90)][SerializeField]float Q2 = 20;
	[Range(0, 90)][SerializeField]float Q3 = 20;
	[Range(0, 90)][SerializeField]float Q4 = 20;
	[Range(1, 80)][SerializeField]int Accuracy = 1;
	[SerializeField]int[] Triangle;
	[SerializeField]Transform target;

	Mesh m_Cone;
	float Q1R, Q2R, Q3R, Q4R;
	public float Q1L, Q2L, Q3L, Q4L;
	public Vector3 toTarget, relativeTarget;
	float distance;
	Vector3 projectPoint3D, newPoint3D;
	Vector2 projectPoint2D, newPoint2D;
	Vector3 targetPosition;
	Transform m_NormalRepresent;
	JointConstraint m_Constraint;

	void OnDrawGizmos() {
		m_NormalRepresent = transform;
		if (m_Constraint == null) 
			m_Constraint = GetComponent<JointConstraint>();
		Start();
		MakeMesh();
		//CheckTarget();

//		Gizmos.color = new Color(0, 1, 1, 0.5f);
//		Gizmos.DrawMesh(m_Cone, transform.position, transform.rotation);
		Gizmos.color = new Color(0, 1, 1, 0.5f);
		Gizmos.DrawWireMesh(m_Cone, transform.position, transform.rotation);
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, target.position);
		Gizmos.color = Color.yellow;
		Gizmos.DrawRay(transform.position, m_Constraint.ConstrainJointPosition(target.position, 2));
		Gizmos.color = Color.magenta;
		Gizmos.DrawRay(transform.position, AngleToPoint(45));
	}

	void Start() {
		Q1R = Q1 * Mathf.Deg2Rad;
		Q2R = Q2 * Mathf.Deg2Rad;
		Q3R = Q3 * Mathf.Deg2Rad;
		Q4R = Q4 * Mathf.Deg2Rad;

		Q1L = Mathf.Sin(Q1R);
		Q2L = Mathf.Sin(Q2R);
		Q3L = Mathf.Sin(Q3R);
		Q4L = Mathf.Sin(Q4R);
		m_NormalRepresent = transform;
		m_Constraint = GetComponent<JointConstraint>();
	}

	void LateUpdate() {
		if (m_Constraint!=null)
			m_Constraint.ConstrainJointPosition(target.position, 3);
		else
			ConstrainTarget(target.position, 3);
	}

	Vector3 ConstrainTarget(Vector3 targetPosition, float desiredDistance) {

		// I will need more knowledge of geometry in order to improve this...eh

		// Get the vector from joint to target.
		toTarget = targetPosition - m_NormalRepresent.position;

		// Get the relative position of target.
		relativeTarget = m_NormalRepresent.InverseTransformPoint(targetPosition);

		// Projects toTarget vector onto the normal of this joint, and gets it's length.
		// For the sake of performance, I simplified some calculation
		// The equation of project:
		//		b/|b1| * a·(b/|b|),  project a onto b, a and b are both vector.
		// But since I just need the length of projection, and b is already an unit vector
		// So the simplified version is merely:
		//		a·b
		//distance = Vector3.Dot(toTarget, transform.rotation * normal);
		distance = toTarget.magnitude;

		// Check if the project point is inside the ellipse defined by Q1, Q2, Q3, and Q4.
		if (isInsideEllipse(distance, relativeTarget)) {
			// Solve for the new point.
			newPoint2D = ProjectPointToEllipseEdge(relativeTarget);
			newPoint3D.Set(newPoint2D.x, newPoint2D.y, 0);

			// We knew how far this point should be...
			newPoint3D.z = Mathf.Sqrt(1 - newPoint2D.sqrMagnitude);
			newPoint3D *= desiredDistance;
		} else {
			newPoint3D = toTarget.normalized * desiredDistance;
		}
			
		return m_NormalRepresent.TransformDirection(newPoint3D);
	}
		
	bool isInsideEllipse(float distance, Vector3 point) {

		if (point.z < 0) return false;

		float a = point.x > 0? Q1L: Q3L;
		float b = point.y > 0? Q2L: Q4L;

		a *= distance;
		b *= distance;

		// From http://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
		float answer = (point.x * point.x) / (a * a) + (point.y * point.y) / (b * b);

		return answer <= 1;
	}

	Vector3 ProjectPointToLine(Vector3 vector, Vector3 normal) {
		// A + dot(AP,AB) / dot(AB,AB) * AB
		// b/|b| * dot(a, (b/|b|))
		return normal * Vector3.Dot(vector, normal);
	}

	Vector2 ProjectPointToEllipseEdge(Vector2 point) {

		// The implicit equation of ellipse is: 
		//		(x^2)/(a^2) + (y^2)/(b^2) = 1
		// Find out which quadrant this point in, and which a and b should be use
		float a = point.x > 0? Q1L: Q3L;
		float b = point.y > 0? Q2L: Q4L;

		// Define a line which cross original point and the target point.
		// The implicit equation of line is: 
		//		y = c*x
		// And then c = y/x
		float c = point.y / point.x;

		// Handle some possible calculation problems.


		// When y = 0...
		if (float.IsNaN(c)) {
			c = 0;
			// When x = 0...
		} else if (float.IsInfinity(c)) {
			c = float.IsPositiveInfinity(c)?  100000: -100000;
		}


		// Solve these two equations for the new point.
		// (x^2)/(a^2) + (y^2)/(b^2) = 1			(1)
		// y = c*x;									(2)
		// 
		// Put (2) in (1) to get (3):
		// (x^2)/(a^2) + ((c*x)^2)/(b^2) = 1		(3)
		//
		// Simplify (3): 
		// x = sqrt((a^2 * b^2)/(b^2 + a^2 * c^2))	(4)
		// 
		// Solve (4), and put x back to (2) to get y
		Vector2 newPoint;
		float powerOfa = a*a;
		float powerOfb = b*b;
		float powerOfc = c*c;
		float u = powerOfb + powerOfa * powerOfc;
		float l = powerOfa * powerOfb;
		float ans = Mathf.Sqrt(l/u);

		// There will be two cross-points between the ellipse and the line,
		// We should find which is the point we need by checking original point.
		newPoint.x = point.x < 0? -ans: ans;

		// And finally, getting y.
		newPoint.y = newPoint.x * c;

		// Return the new point.
		return newPoint;
	}
		
	float AngleWithSign(Vector2 From, Vector2 To) {
		float angle = Vector2.Angle(From, To);
		angle *= Mathf.Sign(From.x * To.y - From.y * To.x);
		return angle;
	}

	// Make a cone mesh, just for editor
	void MakeMesh() {

		if (m_Cone == null) m_Cone = new Mesh();

		Q1R = Q1 * Mathf.Deg2Rad;
		Q2R = Q2 * Mathf.Deg2Rad;
		Q3R = Q3 * Mathf.Deg2Rad;
		Q4R = Q4 * Mathf.Deg2Rad;

		Q1L = Mathf.Sin(Q1R);
		Q2L = Mathf.Sin(Q2R);
		Q3L = Mathf.Sin(Q3R);
		Q4L = Mathf.Sin(Q4R);

		int TotalVert = 1 + 4 * Accuracy;
		m_Vertices = new Vector3[TotalVert];
		float rotateAxisAngle = 90.0f / Accuracy;

		m_Vertices[0] = Vector3.zero;
		for (int i = 1; i < TotalVert; i ++) {
			m_Vertices[i] = AngleToPoint(rotateAxisAngle * (i - 1));
		}

		m_Cone.vertices = m_Vertices;

		Vector3[] normal = new Vector3[TotalVert];
		Triangle = new int[(TotalVert-1) * 3];
		for (int i = 0; i < TotalVert - 1; i ++) {
			Triangle[i * 3] = 0;
			Triangle[i * 3 + 1] = i + 1;
			if (i == TotalVert - 2) {
				Triangle[i * 3 + 2] = 1;
				normal[i + 1] = Vector3.Cross(m_Vertices[1] - m_Vertices[i + 1], m_Vertices[0] - m_Vertices[i + 1]);
			} else {
				Triangle[i * 3 + 2] = i + 2;
				normal[i + 1] = Vector3.Cross(m_Vertices[i + 2] - m_Vertices[i + 1], m_Vertices[0] - m_Vertices[i + 1]);
			}
		}
		normal[0] = -Vector3.forward;
		m_Cone.normals = normal;
		m_Cone.triangles = Triangle;
	}
		
	// This is only use to make a cone mesh
	Vector3 AngleToPoint(float angle) {
		
		Vector3 point = Vector3.zero;
		angle %= 360;
		angle = angle < 0? angle += 360: angle;

		if (angle < 90 || angle >= 270) {
			point.x = Q1L;
		} else {
			point.x = Q3L;
		}

		if (angle < 180) {
			point.y = Q2L;
		} else {
			point.y = Q4L;
		}

		angle *= Mathf.Deg2Rad;
		point.x *= Mathf.Cos(angle);
		point.y *= Mathf.Sin(angle);

		point.z = Mathf.Sqrt(1 - point.sqrMagnitude);
		if (float.IsNaN(point.z)) point.z = 0;

		return point;

	}
}
