﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	public Transform point;
	public Transform axis;
	public float angle;
	public bool affect;

	public Vector3 originalPos;
	public Quaternion originalRot;
	Transform axisRotation;

	// Use this for initialization
	void Start () {
		originalPos = transform.position;
		originalRot = transform.rotation;
		axisRotation = new GameObject("Axis Rotation").transform;
		axisRotation.parent = axis;
		axisRotation.position = axis.position;
		axisRotation.rotation = originalRot;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = originalPos;
		transform.rotation = originalRot;
		if (!affect) return;
		//transform.RotateAround(point.position, axis.up, angle);
		transform.rotation = axisRotation.rotation;
	}
}
